'use strict';

const express = require('express'); 
let dbClient = require('./db');
let app = express();

app.get('/topActiveUsers', retrieveTopUsers);

app.get('/users', retrieveUsers);

app.listen(3000, () => {
	console.log('Running Jobbatical server');
});

function retrieveTopUsers (req, res) {
	let pageNumber = parseInt(req.query.page) || 0;
	let pageSize = 20;
	console.log('retrieveUsers for page %i, paging %i results', pageNumber, pageSize);
	
	dbClient.getActiveUsers(pageNumber, pageSize, (err, users) => {
		console.log('users', users);
		if (err) {
			res.status(500);
			res.send({
				error: 'Could not retrieve user'
			});
		} else {
			res.status(200);
			res.send(users);
		}
	});
}

function retrieveUsers(req, res, next) {
	let id = parseInt(req.query.id) || null;
	console.log('retrieveUsers with id', id);

	if (!id) {
		res.status(400).send({
			error: 'User Id is required as query parameter'
		});
		next();
	} else {
		dbClient.getUserInfo(id, (err, user) => {
			if (err) {
				res.status(500);
				res.send({
					error: err
				});
			} else {
				res.status(200);
				res.send(user);
			}
			next();
		});
	}
}