# Example application for Jobbatical frontend and backend tests
Based on the [Jobbatical backend test](https://bitbucket.org/jobbatical/skilltest-backend/src/73e99b78b2d2?at=master) this application implements the spec.md for querying users' details and top active users.

## Frontend Test

Files are within frontend folder. Suggested to run with [http-server](https://www.npmjs.com/package/http-server)

```
#!bash
$ http-server frontend
```
Then browse http://localhost:8080/
## Backend Test

### Install dependencies

> node install

### Setup your .env file
The following variables are required:


```
#!yaml

user
database
host
password
port
```


e.g.:
```
#!yaml

user=fadlxxxxx
database=fadlxxxx
host=assignment.codsssqklool.eu-central-1.rds.amazonaws.com
password=xyz
port=5432
```

### Run the application
> npm start

Application will start by default in http://localhost:3000