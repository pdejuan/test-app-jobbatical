'use strict';
const pg = require('pg');
const async = require('async');

//load connection settings
require('dotenv').config();
let config = {
	user: process.env.user,
	database: process.env.database,
	host: process.env.host,
	password: process.env.password,
	port: process.env.port
};

let pool = new pg.Pool(config);

/*
TODO: create index for count of activity to make paging more efficient
otherwise the offset will go through all rows until offset.
https://www.postgresql.org/docs/8.1/static/queries-limit.html

@returns array
	id
	name
	count: applications in the last 7 days
	listings: name of the 3 most recent applications 
*/
function getActiveUsers(pageNumber, pageSize, callback) {
	let offset = pageNumber * pageSize;

	let query = 
	`select u.id,
	u.name,
	u.created_at as createdAt,
	( select count(1) 
	from applications a
	where a.user_id = u.id
		and a.created_at > current_date - '7 days'::interval
	) as count,
	(select array_agg(l.name)
	from listings as l, 
		(select *
		from applications
		where user_id = u.id
		order by created_at		
		limit 3) as app
	where l.id = app.listing_id
	) as listings
	from users u
	order by count
	limit $1::integer offset $2::integer`;

	pool.connect( (err,client) => {
		if (err) {
			console.log(err);
			callback('Could not connect to server');
		}

		client.query({
			text: query, 
			values: [pageSize, offset]
		}, (err, result) => {
			if (err) {
				console.log(err);
				callback('Could not connect to query server');
			}
			console.log(result);
			if (result && result.rows) {
				let users = result.rows.map( user => {
					return {
						id: user.id,
						name: user.name,
						count: user.count,
						listings: user.listings || []
					};
				});
				callback(null, users);
			} else {
				callback('No users were found');
			}
		});
	});
}

function getBasicInfo(id, client, callback) {
	let query_user = `
	select id, name, created_at
	from users u 
	where u.id = $1::integer
	`;
	client.query(query_user, [id], (err, results) => {
		if (err) {
			console.log(err);
			callback('Could not retrieve basic info');
		}
		if (results && results.rows && results.rows.length) {
			let user = results.rows[0];
			callback(null, {
				id: user.id,
				name: user.name,
				createdAt: user.created_at
			});
		} else {
			callback(`User ${id} not found`);
		}
	});
}

function getListings(id, client, callback) {
	let query_listings = `
	select l.id, l.created_at, l.name, l.description 
		from listings l
	where created_by = $1::integer`;
	client.query(query_listings, [id], (err, results) => {
		if (err) {
			callback(err);
		}
		if (results && results.rows) {
			let listings = results.rows.map( (list) => {
				return {
					id: list.id,
					createdAt: list.create_at,
					name: list.name,
					description: list.description
				};
			});
			callback(null, listings);
		}
	});
}

function getCompanies(id, client, callback) {
	let query_companies = `
	select c.id, c.name, c.created_at, contact_user
		from companies c, teams t
	where c.id = t.company_id
		and t.user_id = $1::integer`;

	client.query(query_companies, [id], (err, results) => {
		if (err) {
			callback(err);
		}
		let companies = [];
		if (results && results.rows) {
			companies = results.rows.map( (company)  => {
				return {
					id: company.id,
					createdAt: company.create_at,
					name: company.name,
					isContact: company.contact_user
				};
			});
		}
		callback(null, companies);
	});
}

function getApplications(id, client, callback) {
	let query_applications = `
	select a.id, a.created_at, a.cover_letter,
		l.id as listing_id, l.name as listing_name,
		l.description as listing_description
	from applications a, listings l
	where a.listing_id = l.id 
		and user_id =$1::integer`;

	client.query(query_applications, [id], (err, results) => {
		if (err) {
			callback(err);
		}
		if (results && results.rows) {
			let apps = results.rows.map( (app) => {
				return {
					id: app.id,
					createdAt: app.create_at,
					listing: {
						id: app.listing_id,
						name: app.listing_name,
						description: app.listing_description
					}
				};
			});
			callback(null, apps);
		}
	});
}

function getUserDetails (id, callback) {
	pool.connect( (err,client) => {
		if (err) {
			console.log(err);
			callback('Could not connect to server');
		}
		getBasicInfo(id, client, (err, user) => {
			if (err){
				console.log(err);
				callback(err);
			}
			//callback(null, user);
			async.parallel({
				companies: (callback) => {
					getCompanies(id, client, callback);
				},
				listings: (callback) => {
					getListings(id, client, callback);
				},
				applications: (callback) => {
					getApplications(id, client, callback);
				}
			}, (err, results) => {
				if (err) {
					console.log(err);
					callback('Could not query user info');
				}
				user.companies = results.companies;
				user.createdListings = results.listings;
				user.applications = results.applications;
				callback(null, user);
			});
		});
	});
}

module.exports = {
	getUserInfo: getUserDetails,
	getActiveUsers: getActiveUsers
};